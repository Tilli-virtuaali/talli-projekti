var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PatColor Model
 * ==========
 */

var PatColor = new keystone.List('PatColor', {
	map: { name: 'colorTerm' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

PatColor.add({
	colorTerm: { type: String, required: true },
	colorTermShort: { type: String, label: 'Term abbreviation' },
	colorGenes: { 
	    sabiano: { type: Types.Number, label: 'Sabiano' },
	    tobiano: { type: Types.Number, label: 'Tobiano' },
	    overo: { type: Types.Number, label: 'Overo' }
	}
});

PatColor.relationship({ ref: 'Horse', path: 'patcolor', refPath: 'color.patColor' });

PatColor.defaultColumns = 'colorTerm, colorTermShort, colorGene';
PatColor.register();