var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * BaseColor Model
 * ==========
 */

var BaseColor = new keystone.List('BaseColor', {
	map: { name: 'colorTerm' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

BaseColor.add({
	colorTerm: { type: String, required: true },
	colorTermShort: { type: String, label: 'Term abbreviation' },
	colorGenes: { 
	    agouti: { type: String, label: 'Agouti (A)' },
	    extension: { type: String, label: 'Extension (E)' },
		white: { type: String, label: 'Dominant white (W)' },
	} 
});

BaseColor.relationship({ ref: 'Horse', path: 'basecolor', refPath: 'color.baseColor' });

BaseColor.defaultColumns = 'colorTerm, colorTermShort, colorGenes.agouti, colorGenes.extension, colorGenes.white';
BaseColor.register();
