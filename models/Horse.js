var keystone = require('keystone');
var request = require('request');
var Types = keystone.Field.Types;

/**
 * Horse Profile Model
 * ==========
 */

var Horse = new keystone.List('Horse', {
  map: { name: 'title' },
  autokey: { path: 'slug', from: 'title', unique: true },
  label: 'Horses'
});

Horse.add({
  title: { type: String, required: true },
  stock: { type: Types.Boolean, default: true },
  state: { type: Types.Select, options: 'draft, published, archived', default: 'draft', index: true },
  status: { type: Types.Select, options: 'foal, yearling, adult, senior, dead', default: 'adult', index: true },
  owner: { type: Types.Relationship, ref: 'User', index: true },
  birthDay: { type: Types.Date, index: true },
  breed: { type: String },
  gender: { type: Types.Select, options: 'gelding, stallion, mare'},
  withersHeight: { type: String },
  markings: { type: String },
  generation: { type: Types.Number, default: 1 }, // 1: only generated points, nothing inherited, 2: some points inherited, 3: inherited points, 4: inherited points and training
  color: {
    baseColor: { type: Types.Relationship, ref: 'BaseColor', label: 'Base color', index: true },
    modColor: { type: Types.Relationship, ref: 'ModColor', label: 'Modification color', index: true },
    patColor: { type: Types.Relationship, ref: 'PatColor', label: 'Pattern', index: true }
  },
  breeder: { type: String },
  createdDate: { type: Types.Date, hidden: true },
  content: {
    extended: { type: Types.Html, wysiwyg: true, height: 400 }
  },
  pedigree: {
    sire: { type: Types.Relationship, ref: 'Horse' },
    dam: { type: Types.Relationship, ref: 'Horse' }
  },
  character: {
    temperament: { type: Types.Number, label: 'Temperament', noedit: true },
    obedience: { type: Types.Number, label: 'Obedience', noedit: true },
    nerves: { type: Types.Number, label: 'nerves', noedit: true },
    maneuverability: { type: Types.Number, label: 'Maneuverability', noedit: true },
    curiosity: { type: Types.Number, label: 'Curiosity', noedit: true },
    humor: { type: Types.Number, label: 'Humor', noedit: true },
    patience: { type: Types.Number, label: 'Patience', noedit: true }
  },
  skills: {
    speed: { type: Types.Number, label: 'Speed', noedit: true },
    endurance: { type: Types.Number, label: 'Endurance', noedit: true },
    agility: { type: Types.Number, label: 'Agility', noedit: true }
  },
  conformation: {
    head: { type: Types.Number, label: 'Head', noedit: true},
    neck: { type: Types.Number, label: 'Neck', noedit: true},
    withers: { type: Types.Number, label: 'Withers', noedit: true},
    back: { type: Types.Number, label: 'Back', noedit: true},
    shoulders: { type: Types.Number, label: 'Shoulders', noedit: true},
    legs: { type: Types.Number, label: 'Legs', noedit: true},
    hooves: { type: Types.Number, label: 'Hooves', noedit: true}
  }
});

Horse.schema.virtual('content.full').get(function() {
  return this.content.extended || this.content.brief;
});

Horse.defaultColumns = 'title, state|20%, owner|20%, publishedDate|20%';


// Creating method for publish date 
Horse.schema.methods.isPublished = function() {
    return this.state == 'published';
}

// Update created date and genome when horse is first time published 
Horse.schema.pre('save', function(next) {
    if (this.isModified('state') && this.isPublished() && !this.createdDate) {
       
        this.createdDate = new Date();
        
        console.log('get randomized genome: -------------');
      
        var new_horse = this;
        
        var skillsKeys = Object.keys(new_horse.skills);
        var characterKeys = Object.keys(new_horse.character);
        var conformationKeys = Object.keys(new_horse.conformation);
    
        var options = { 
                    method: 'POST',
                    url: 'https://virtuaalimaailma-anniina.c9users.io/api/horse/create',
                    headers: 
                    { 'content-type': 'application/x-www-form-urlencoded',
                     'postman-token': 'a766d45f-ccc1-da7c-20d8-35d20e66c62f',
                     'cache-control': 'no-cache' },
                    form: { title: this.title, state: 'published' } 
                };
                        
        request(options, function (error, response, body) {
            if (error) throw new Error(error);
        
            var data = JSON.parse(body);
            
            skillsKeys.forEach(function (item, index, array) {
              if(item != 'toObject' && item != 'toJSON' && !new_horse.skills[item]) {
                new_horse.skills[item] = data.horse.skills[item];
              }
            });
            
            characterKeys.forEach(function (item, index, array) {
              if(item != 'toObject' && item != 'toJSON' && !new_horse.character[item]) {
                new_horse.character[item] = data.horse.character[item];
              }
            });
            
            conformationKeys.forEach(function (item, index, array) {
              if(item != 'toObject' && item != 'toJSON' && !new_horse.conformation[item]) {
                new_horse.conformation[item] = data.horse.conformation[item];
              }
            });
            
            next();
        });
        
    } else {
        next();
    }
});


Horse.register();