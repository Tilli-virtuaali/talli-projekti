var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ModColor Model
 * ==========
 */

var ModColor = new keystone.List('ModColor', {
	map: { name: 'colorTerm' },
	autokey: { path: 'slug', from: 'title', unique: true },
});

ModColor.add({
	colorTerm: { type: String, required: true },
	colorTermShort: { type: String, label: 'Term abbreviation' },
	colorGenes: { 
	    cream: { type: String, label: 'Cream (C)' },
	    dun: { type: String, label: 'Dun (D)' },
	    champagne: { type: String, label: 'Champagne (Ch)' },
	    silver: { type: String, label: 'Silver (Z)' },
	    pearl: { type: String, label: 'Pearl (Pr)' },
	    roan: { type: String, label: 'Roan (Rn)' }
	}
});

ModColor.relationship({ ref: 'Horse', path: 'modcolor', refPath: 'color.modColor' });

ModColor.defaultColumns = 'colorTerm, colorTermShort, colorGene';
ModColor.register();